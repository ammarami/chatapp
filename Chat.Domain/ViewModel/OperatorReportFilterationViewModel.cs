﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperatorReport.Models
{
    public class OperatorReportFilterationViewModel
    {
        public string Website { get; set; }
        public string Device { get; set; }
        public string PreDefinedVal { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}