﻿using System.ComponentModel.DataAnnotations;

namespace Chat.Domain.Entities
{
    public partial class vw_LookupWebsite
    {
        [Key]
        public string Website { get; set; }

    }
}