﻿using System.ComponentModel.DataAnnotations;

namespace Chat.Domain.Entities
{
    public partial class vw_LookupPreDefined
    {
        [Key]
        public string PreDefinedText { get; set; }
        public string PreDefinedValue { get; set; }

    }
}