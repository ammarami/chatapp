﻿using System.ComponentModel.DataAnnotations;

namespace Chat.Domain.Entities
{
    public partial class vw_LookupDevice
    {
        [Key]
        public string Device { get; set; }

    }
}