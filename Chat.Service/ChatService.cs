using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Chat.Data;
using Chat.Data.Infrastructure;
using Chat.Domain.Entities;
using OperatorReport.Models;

namespace Chat.Service
{
    public partial class ChatService : IChatService
    {
        #region Initialization
        private readonly Ivw_LookupDeviceRepository vw_LookupDeviceRepository;
        private readonly Ivw_LookupWebsiteRepository vw_LookupWebsiteRepository;
        private readonly Ivw_LookupPreDefinedRepository vw_LookupPreDefinedRepository;
        private readonly IUnitOfWork unitOfWork;
        
        public ChatService(
            Ivw_LookupDeviceRepository vw_LookupDeviceRepository,
            Ivw_LookupWebsiteRepository vw_LookupWebsiteRepository,
            Ivw_LookupPreDefinedRepository vw_LookupPreDefinedRepository,
        IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;           
            this.vw_LookupDeviceRepository = vw_LookupDeviceRepository;
            this.vw_LookupWebsiteRepository = vw_LookupWebsiteRepository;
            this.vw_LookupPreDefinedRepository = vw_LookupPreDefinedRepository;
        }

        public void Save(int userId)
        {
            unitOfWork.Commit(userId);
        }

        #endregion Initialization

        #region vw_LookupDevice

        public IEnumerable<vw_LookupDevice> SelectAllvw_LookupDevices()
        {
            return vw_LookupDeviceRepository.SelectAll();
        }

        public IEnumerable<vw_LookupDevice> SelectMany_vw_LookupDevice(Expression<Func<vw_LookupDevice, bool>> where)
        {
            return vw_LookupDeviceRepository.SelectMany(where);
        }

        public vw_LookupDevice SelectSingle_vw_LookupDevice(Expression<Func<vw_LookupDevice, bool>> where)
        {
            return vw_LookupDeviceRepository.Select(where);
        }

        #endregion vw_LookupDevice

        #region vw_LookupWebsite

        public IEnumerable<vw_LookupWebsite> SelectAllvw_LookupWebsites()
        {
            return vw_LookupWebsiteRepository.SelectAll();
        }

        public IEnumerable<vw_LookupWebsite> SelectMany_vw_LookupWebsite(Expression<Func<vw_LookupWebsite, bool>> where)
        {
            return vw_LookupWebsiteRepository.SelectMany(where);
        }

        public vw_LookupWebsite SelectSingle_vw_LookupWebsite(Expression<Func<vw_LookupWebsite, bool>> where)
        {
            return vw_LookupWebsiteRepository.Select(where);
        }

        #endregion vw_LookupWebsite

        #region vw_LookupPreDefined

        public IEnumerable<vw_LookupPreDefined> SelectAllvw_LookupPreDefineds()
        {
            return vw_LookupPreDefinedRepository.SelectAll();
        }

        public IEnumerable<vw_LookupPreDefined> SelectMany_vw_LookupPreDefined(Expression<Func<vw_LookupPreDefined, bool>> where)
        {
            return vw_LookupPreDefinedRepository.SelectMany(where);
        }

        public vw_LookupPreDefined SelectSingle_vw_LookupPreDefined(Expression<Func<vw_LookupPreDefined, bool>> where)
        {
            return vw_LookupPreDefinedRepository.Select(where);
        }

        #endregion vw_LookupPreDefined
        public List<OperatorReportViewModel> sp_OperatorProductivity(OperatorReportFilterationViewModel model)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(model.PreDefinedVal))
                {
                    if(model.PreDefinedVal == "1")
                    {
                        //Today
                        model.StartDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 00, 00, 00);
                        model.EndDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 23, 59, 59);
                    }
                    else if(model.PreDefinedVal == "2")
                    {
                        //Yesterday
                        model.StartDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.AddDays(-1).Day, 00, 00, 00);
                        model.EndDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.AddDays(-1).Day, 23, 59, 59);
                    }
                    else if (model.PreDefinedVal == "3")
                    {
                        //This Week
                        model.StartDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.AddDays(-6).Day, 00, 00, 00);
                        model.EndDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 23, 59, 59);
                    }
                    else if (model.PreDefinedVal == "4")
                    {
                        //Last Week
                        model.StartDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.AddDays(-12).Day, 00, 00, 00);
                        model.EndDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.AddDays(-6).Day, 23, 59, 59);
                    }
                    else if (model.PreDefinedVal == "5")
                    {
                        //This Month
                        DateTime now = DateTime.UtcNow;
                        model.StartDate = new DateTime(now.Year, now.Month, 1);
                        model.EndDate = model.StartDate.Value.AddMonths(1).AddDays(-1);
                    }
                    else if (model.PreDefinedVal == "6")
                    {
                        //Last Month
                        var today = DateTime.Today;
                        var month = new DateTime(today.Year, today.Month, 1);
                        model.StartDate = month.AddMonths(-1);
                        model.EndDate = month.AddDays(-1);
                    }
                    else if (model.PreDefinedVal == "7")
                    {
                        //This Year
                        DateTime now = DateTime.UtcNow;
                        model.StartDate = new DateTime(now.Year, 01, 01);
                        model.EndDate = model.StartDate.Value.AddMonths(12);
                    }
                    else if (model.PreDefinedVal == "8")
                    {
                        //Last Year
                        DateTime now = DateTime.UtcNow;
                        model.StartDate = new DateTime(now.AddYears(-1).Year, 01, 01);
                        model.EndDate = model.StartDate.Value.AddMonths(12);
                    }
                }
                var result = vw_LookupWebsiteRepository.sp_OperatorProductivity(model);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    public partial interface IChatService
    {
        // Interface Methods 

        #region vw_LookupDevice

        IEnumerable<vw_LookupDevice> SelectAllvw_LookupDevices();

        IEnumerable<vw_LookupDevice> SelectMany_vw_LookupDevice(Expression<Func<vw_LookupDevice, bool>> where);

        vw_LookupDevice SelectSingle_vw_LookupDevice(Expression<Func<vw_LookupDevice, bool>> where);

        #endregion

        #region vw_LookupWebsite

        IEnumerable<vw_LookupWebsite> SelectAllvw_LookupWebsites();

        IEnumerable<vw_LookupWebsite> SelectMany_vw_LookupWebsite(Expression<Func<vw_LookupWebsite, bool>> where);

        vw_LookupWebsite SelectSingle_vw_LookupWebsite(Expression<Func<vw_LookupWebsite, bool>> where);

        #endregion

        #region vw_LookupPreDefined

        IEnumerable<vw_LookupPreDefined> SelectAllvw_LookupPreDefineds();

        IEnumerable<vw_LookupPreDefined> SelectMany_vw_LookupPreDefined(Expression<Func<vw_LookupPreDefined, bool>> where);

        vw_LookupPreDefined SelectSingle_vw_LookupPreDefined(Expression<Func<vw_LookupPreDefined, bool>> where);

        #endregion
        List<OperatorReportViewModel> sp_OperatorProductivity(OperatorReportFilterationViewModel model);
    }
}

