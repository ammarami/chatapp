using Chat.Domain.Entities;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;

namespace Chat.Data
{
    public class AppContext : DbContext
    {
        public AppContext()
            : base("name=AppContext")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<AppContext>());
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 300;
        }
        public DbSet<vw_LookupDevice> vw_LookupDevices { get; set; }
        public DbSet<vw_LookupPreDefined> vw_LookupPreDefineds { get; set; }
        
        public DbSet<vw_LookupWebsite> vw_LookupWebsites { get; set; }
        public virtual void Commit(int currentUserId)
        {
            SaveChanges(currentUserId);
        }

        public int SaveChanges(int currentUserId)
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw new Exception("The record you attempted to save was modified by another user after you load the original record. " +
                    " The save operation was canceled. Please reload form.");
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        throw new Exception(string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return 0;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

    public class AppContextInitializer : DropCreateDatabaseIfModelChanges<AppContext>
    {
        protected override void Seed(AppContext context)
        {
            //
        }
    }
}
