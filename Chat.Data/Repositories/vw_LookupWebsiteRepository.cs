using Chat.Domain.Entities;
using Chat.Data.Infrastructure;
using System.Data.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using OperatorReport.Models;
using System.Collections.Generic;

namespace Chat.Data
{
    public partial class vw_LookupWebsiteRepository : RepositoryBase<vw_LookupWebsite>, Ivw_LookupWebsiteRepository
    {
        public vw_LookupWebsiteRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            //
        }
        public List<OperatorReportViewModel> sp_OperatorProductivity(OperatorReportFilterationViewModel model)
        {
            List<OperatorReportViewModel> result = new List<OperatorReportViewModel>();
            try
            {
                this.DataContext.Database.Connection.Open();
                DbCommand cmd = DataContext.Database.Connection.CreateCommand();
                cmd.CommandText = "OperatorProductivity";
                cmd.CommandType = CommandType.StoredProcedure;

                if(model.StartDate.HasValue && model.EndDate.HasValue)
                {
                    cmd.Parameters.Add(new SqlParameter("StartDate", model.StartDate.Value.ToString("yyyy-MM-dd")));
                    cmd.Parameters.Add(new SqlParameter("EndDate", model.EndDate.Value.ToString("yyyy-MM-dd")));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("StartDate", null));
                    cmd.Parameters.Add(new SqlParameter("EndDate", null));
                }
                cmd.Parameters.Add(new SqlParameter("Website", model.Website));
                cmd.Parameters.Add(new SqlParameter("Device", model.Device));

                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader != null) {
                        while (reader.Read())
                        {
                            OperatorReportViewModel obj = new OperatorReportViewModel();
                            obj.ID = reader.IsDBNull(0) ? 0 : Convert.ToInt32(reader.GetValue(0));
                            obj.Name = reader.IsDBNull(1) ? "-" : reader.GetValue(1).ToString();
                            obj.ProactiveSent = reader.IsDBNull(2) ? 0 : Convert.ToInt32(reader.GetValue(2));
                            obj.ProactiveAnswered = reader.IsDBNull(3) ? 0 : Convert.ToInt32(reader.GetValue(3));
                            obj.ProactiveResponseRate = reader.IsDBNull(4) ? 0 : Convert.ToInt32(reader.GetValue(4));
                            obj.ReactiveReceived = reader.IsDBNull(5) ? 0 : Convert.ToInt32(reader.GetValue(5));
                            obj.ReactiveAnswered = reader.IsDBNull(6) ? 0 : Convert.ToInt32(reader.GetValue(6));
                            obj.ReactiveResponseRate = reader.IsDBNull(7) ? 0 : Convert.ToInt32(reader.GetValue(7));
                            obj.TotalChatLength = reader.IsDBNull(8) ? "-" : reader.GetValue(8).ToString();
                            obj.AverageChatLength = reader.IsDBNull(9) ? "-" : reader.GetValue(9).ToString();
                            result.Add(obj);
                        }
                    }
                }
                    
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.DataContext.Database.Connection.Close();
            }
        }

    }
    public partial interface Ivw_LookupWebsiteRepository : IRepository<vw_LookupWebsite>
    {
        List<OperatorReportViewModel> sp_OperatorProductivity(OperatorReportFilterationViewModel model);

    }
}

