using Chat.Domain.Entities;
using Chat.Data.Infrastructure;

namespace Chat.Data
{
    public partial class vw_LookupDeviceRepository : RepositoryBase<vw_LookupDevice>, Ivw_LookupDeviceRepository
    {
        public vw_LookupDeviceRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            //
        }
        
    }
    public partial interface Ivw_LookupDeviceRepository : IRepository<vw_LookupDevice>
    {
    }
}

