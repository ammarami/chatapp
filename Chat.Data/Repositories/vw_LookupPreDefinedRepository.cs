using Chat.Domain.Entities;
using Chat.Data.Infrastructure;

namespace Chat.Data
{
    public partial class vw_LookupPreDefinedRepository : RepositoryBase<vw_LookupPreDefined>, Ivw_LookupPreDefinedRepository
    {
        public vw_LookupPreDefinedRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
            //
        }
        
    }
    public partial interface Ivw_LookupPreDefinedRepository : IRepository<vw_LookupPreDefined>
    {
    }
}

