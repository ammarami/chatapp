﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chat.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit(int currentUserId);
    }
}
