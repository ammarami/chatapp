﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Chat.Domain;
using System.Data;
using System.Linq.Expressions;

namespace Chat.Data.Infrastructure
{
    public abstract class RepositoryBase<T> where T : class
    {
        private AppContext dataContext;
        private readonly IDbSet<T> dbset;

        public string ExceptionDetails { get; set; }

        protected RepositoryBase(IDatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
            dbset = DataContext.Set<T>();
        }

        protected IDatabaseFactory DatabaseFactory
        {
            get;
            private set;
        }

        protected AppContext DataContext
        {
            get { return dataContext ?? (dataContext = DatabaseFactory.Get()); }
        }

        public virtual void Add(T entity)
        {
            dbset.Add(entity);
        }

        public virtual void Update(T entity)
        {
            dbset.Attach(entity);
            dataContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            dbset.Remove(entity);
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = dbset.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                dbset.Remove(obj);
        }

        public virtual T SelectById(long id)
        {
            return dbset.Find(id);
        }

        public virtual T SelectById(string id)
        {
            return dbset.Find(id);
        }

        public virtual IEnumerable<T> SelectAll()
        {
            return dbset.ToList();
        }

        public virtual IEnumerable<T> SelectMany(Expression<Func<T, bool>> where)
        {
            return dbset.Where(where).ToList();
        }

        public T Select(Expression<Func<T, bool>> where)
        {

            return dbset.Where(where).FirstOrDefault<T>();
        }

        private string MapToEntityColumnName(string columnName)
        {
            if (columnName.Contains("_"))
            {
                return columnName.Substring(columnName.IndexOf("_") + 1);
            }
            else
            {
                return columnName;
            }
        }


    }
}
