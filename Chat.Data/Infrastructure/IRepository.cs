﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace Chat.Data.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> where);
        T SelectById(long Id);
        T SelectById(string Id);
        T Select(Expression<Func<T, bool>> where);
        IEnumerable<T> SelectAll();
        IEnumerable<T> SelectMany(Expression<Func<T, bool>> where);
    }
}
