﻿using System.Web;
using System.Web.Optimization;

namespace Chat.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.4.1.js",
                        "~/Scripts/moment.min.js",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/bootstrap-datetimepicker.min.js",
                        "~/Scripts/jquery.dataTables.min.js",
                        "~/Scripts/ataTables.jqueryui.min.js",
                        "~/Scripts/dataTables.scroller.min.js",
                        "~/Scripts/notify.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                    "~/Scripts/app/ChatJS.js"
                     ));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/dataTables.jqueryui.min.css",
                      "~/Content/scroller.jqueryui.min.css",
                      "~/Content/site.css"));

        }
    }
}
