﻿using Chat.Helpers;
using Chat.Service;
using Chat.Web.ViewModel;
using OperatorReport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Chat.Web.Controllers
{
    public class HomeController : Controller
    {
        #region Initialization

        private readonly IChatService chatService;

        public HomeController(IChatService chatService)
        {
            this.chatService = chatService;
        }

        #endregion Initialization

        #region Filteration

        [HttpGet]
        public ActionResult Index()
        {
            IndexViewModel model = new IndexViewModel();
            OperatorReportFilterationViewModel filterModel = new OperatorReportFilterationViewModel();
            model.LoadDevices(chatService.SelectAllvw_LookupDevices().OrderBy(x => x.Device));
            model.LoadWebsites(chatService.SelectAllvw_LookupWebsites().OrderBy(x => x.Website));
            model.LoadPreDefineds(chatService.SelectAllvw_LookupPreDefineds());
            var data = chatService.sp_OperatorProductivity(filterModel);
            model.ReportData = (from x in data
                                select new Report
                                {
                                    ID = x.ID.ToString(),
                                    Name = x.Name,
                                    ProactiveSent = FormatValuesExtention.GetFormattedValue(x.ProactiveSent),
                                    ProactiveAnswered = FormatValuesExtention.GetFormattedValue(x.ProactiveAnswered),
                                    ProactiveResponseRate = FormatValuesExtention.GetPercentageFormattedValue(x.ProactiveResponseRate),
                                    ReactiveReceived = FormatValuesExtention.GetFormattedValue(x.ReactiveReceived),
                                    ReactiveAnswered = FormatValuesExtention.GetFormattedValue(x.ReactiveAnswered),
                                    ReactiveResponseRate = FormatValuesExtention.GetPercentageFormattedValue(x.ReactiveResponseRate),
                                    TotalChatLength = FormatValuesExtention.GetMinutesFormattedValue(x.TotalChatLength),
                                    AverageChatLength = FormatValuesExtention.GetMinutesFormattedValue(x.AverageChatLength)
                                }).ToList();

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(IndexViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return new AjaxableViewResult(model);
            }
            model.LoadDevices(chatService.SelectAllvw_LookupDevices().OrderBy(x => x.Device));
            model.LoadWebsites(chatService.SelectAllvw_LookupWebsites().OrderBy(x => x.Website));
            model.LoadPreDefineds(chatService.SelectAllvw_LookupPreDefineds());
            OperatorReportFilterationViewModel filterModel = new OperatorReportFilterationViewModel();
            if (model.StartDate.HasValue && model.EndDate.HasValue)
            {
                filterModel.StartDate = model.StartDate.Value;
                filterModel.EndDate = model.EndDate.Value;
            }
            else
            {
                filterModel.PreDefinedVal = model.PreDefinedVal;
            }
            filterModel.Website = model.Website;
            filterModel.Device = model.Device;
            var data = chatService.sp_OperatorProductivity(filterModel);
            model.ReportData = (from x in data
                                select new Report
                                {
                                    ID = x.ID.ToString(),
                                    Name = x.Name,
                                    ProactiveSent = FormatValuesExtention.GetFormattedValue(x.ProactiveSent),
                                    ProactiveAnswered = FormatValuesExtention.GetFormattedValue(x.ProactiveAnswered),
                                    ProactiveResponseRate = FormatValuesExtention.GetPercentageFormattedValue(x.ProactiveResponseRate),
                                    ReactiveReceived = FormatValuesExtention.GetFormattedValue(x.ReactiveReceived),
                                    ReactiveAnswered = FormatValuesExtention.GetFormattedValue(x.ReactiveAnswered),
                                    ReactiveResponseRate = FormatValuesExtention.GetPercentageFormattedValue(x.ReactiveResponseRate),
                                    TotalChatLength = FormatValuesExtention.GetMinutesFormattedValue(x.TotalChatLength),
                                    AverageChatLength = FormatValuesExtention.GetMinutesFormattedValue(x.AverageChatLength)
                                }).ToList();

            return View(model);
        }

        #endregion Filteration

    }
}