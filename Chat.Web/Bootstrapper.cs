using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using Chat.Data;
using Chat.Data.Infrastructure;
using Chat.Service;
using Chat.Web.IoC;

namespace Chat.Web
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();            

            container
            .RegisterType<IDatabaseFactory, DatabaseFactory>(new HttpContextLifetimeManager<IDatabaseFactory>())
            .RegisterType<IUnitOfWork, UnitOfWork>(new HttpContextLifetimeManager<IUnitOfWork>())

            // Services
            .RegisterType<IChatService, ChatService>(new HttpContextLifetimeManager<IChatService>())

            // Tables
            .RegisterType<Ivw_LookupDeviceRepository, vw_LookupDeviceRepository>(new HttpContextLifetimeManager<Ivw_LookupDeviceRepository>())
            .RegisterType<Ivw_LookupWebsiteRepository, vw_LookupWebsiteRepository>(new HttpContextLifetimeManager<Ivw_LookupWebsiteRepository>())
            .RegisterType<Ivw_LookupPreDefinedRepository, vw_LookupPreDefinedRepository>(new HttpContextLifetimeManager<Ivw_LookupPreDefinedRepository>());
            return container;
        }
    }
}