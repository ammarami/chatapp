﻿using System.Web.WebPages.Html;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Chat.Domain.Entities;
using System.Linq;
using OperatorReport.Models;
using System;

namespace Chat.Web.ViewModel
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            Devices = new List<SelectListItem>();
            Websites = new List<SelectListItem>();
            PreDefineds = new List<SelectListItem>();
        }
        [Display(Name = "Websites")]
        public string Website { get; set; }
        public IEnumerable<SelectListItem> Websites { get; set; }
        public void LoadWebsites(IEnumerable<vw_LookupWebsite> data)
        {
            Websites = data.Select(item => new SelectListItem { Value = item.Website, Text = item.Website }).ToList();
        }
        [Display(Name = "Devices")]
        public string Device { get; set; }
        public IEnumerable<SelectListItem> Devices { get; set; }
        public void LoadDevices(IEnumerable<vw_LookupDevice> data)
        {
            Devices = data.Select(item => new SelectListItem { Value = item.Device, Text = item.Device }).ToList();
        }

        [Display(Name = "Pre Defined")]
        public string PreDefinedVal { get; set; }
        public IEnumerable<SelectListItem> PreDefineds { get; set; }
        public void LoadPreDefineds(IEnumerable<vw_LookupPreDefined> data)
        {
            PreDefineds = data.Select(item => new SelectListItem { Value = item.PreDefinedValue, Text = item.PreDefinedText }).ToList();
        }
        public List<Report> ReportData { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
    public class Report {
        public string ID { get; set; }
        public string Name { get; set; }
        public string ProactiveSent { get; set; }
        public string ProactiveAnswered { get; set; }
        public string ProactiveResponseRate { get; set; }
        public string ReactiveReceived { get; set; }
        public string ReactiveAnswered { get; set; }
        public string ReactiveResponseRate { get; set; }
        public string TotalChatLength { get; set; }
        public string AverageChatLength { get; set; }
    }
}