﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;

namespace Chat.Helpers
{
    public static class FormatValuesExtention
    {
        public static string GetFormattedValue(this int? value)
        {
            string result = string.Empty;
            if (value.HasValue && value != 0)
            {
                result = value.Value.ToString("d");
            }
            else
            {
                result = "-";
            }

            return result;
        }
        public static string GetPercentageFormattedValue(this int? value)
        {
            string result = string.Empty;
            if (value.HasValue && value != 0)
            {
                result = value.Value.ToString("d") + "%";
            }
            else
            {
                result = "-";
            }

            return result;
        }
        public static string GetMinutesFormattedValue(this string value)
        {
            string result = string.Empty;
            if (!string.IsNullOrWhiteSpace(value) && value != "-")
            {
                int totalMinutes = Convert.ToInt32(value);
                TimeSpan t = TimeSpan.FromMinutes(totalMinutes);
                if (totalMinutes >= 1440)
                {
                    result = string.Format("{0:D2}d {1:D2}h {2:D2}m {3:D2}s",
                                    t.Days,
                                    t.Hours,
                                    t.Minutes,
                                    t.Seconds
                                    );
                }
                else if (totalMinutes < 1440 && totalMinutes >= 60)
                {
                    result = string.Format("{0:D2}h {1:D2}m {2:D2}s",
                                        t.Hours,
                                        t.Minutes,
                                        t.Seconds
                                        );
                }
                else if (totalMinutes < 60 && totalMinutes > 1)
                {
                    result = string.Format("{0:D2}m {1:D2}s",
                                        t.Minutes,
                                        t.Seconds
                                        );
                }
                else
                {
                    result = string.Format("{0:D2}s",
                                    t.Seconds
                                    );
                }

            }
            else
            {
                result = "-";
            }

            return result;
        }
    }
}